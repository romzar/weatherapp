import { MODAL_FORM_ACTIONS } from './constants';

export class ModalFormActions {
  static openModal      = (type) => ({
    type: type,
  });
  static closeModalForm = () => ({
    type: MODAL_FORM_ACTIONS.CLOSE_MODAL_FORM,
  });
}
export const openModal      = (type) => ({
  type: type,
});

export const closeModalForm = () => ({
  type: MODAL_FORM_ACTIONS.CLOSE_MODAL_FORM,
});