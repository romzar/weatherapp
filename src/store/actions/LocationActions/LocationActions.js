import { APIs } from '../../../lib/Api';

export class LocationActions{
  static getRegions(){
    return APIs.UserLocationApi.getRegions()
  }
  static getCities(regionId, suggestion='',offset=0){
    return APIs.UserLocationApi.getCities(regionId, suggestion, offset);
  }
}