import { toast }           from 'react-toastify';
import { APIs }            from '../../../lib/Api';
import { WEATHER_ACTIONS } from './constants';

export class WeatherActions {
  static getForecastStart = cityName => {
    return {
      type: WEATHER_ACTIONS.GET_FORECAST_START,
      city: cityName,
    };
  };
  
  static getForecastSuccess = response => {
    return {
      type:    WEATHER_ACTIONS.GET_FORECAST_SUCCESS,
      payload: response,
    };
  };
  
  static getForecastFail = () => {
    console.log('Forecast fail');
    return {
      type: WEATHER_ACTIONS.GET_FORECAST_FAIL,
    };
  };
  
  static fetchWeather = capital => {
    return async dispatch => {
      const {getForecastStart, getForecastSuccess, getForecastFail} = WeatherActions;
      dispatch(getForecastStart(capital));
      try {
        const api      = APIs.WeatherAPI;
        const response = await api.getWeatherForecast(capital);
        dispatch(getForecastSuccess(response));
      } catch (e) {
        toast.error('Error while fetching weather');
        dispatch(getForecastFail());
      }
    };
  };
}