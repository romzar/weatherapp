import { toast }            from 'react-toastify';
import { APIs }             from '../../../lib/Api';
import { LocalStorage }     from '../../../lib/LocalStorage';
import { ModalFormActions } from '../ModalFormActions';
import { USER_ACTIONS }     from './constants';

export class UserActions {
  static  authUser = payload => ({
    type:    USER_ACTIONS.AUTH_USER,
    payload,
  });
  
  static purgeUser = () => {
    LocalStorage.setAuthToken('');
    return {
      type: USER_ACTIONS.PURGE_USER,
      payload: []
    };
  };
  
  static getCurrentUserInfo = () => async dispatch => {
    try {
      let response = await APIs.UserApi.getCurrentUserInfo();
      dispatch(UserActions.authUser(response.data));
    } catch (e) {
      if (e.status === 401) {
        dispatch(UserActions.purgeUser());
      }
    }
  };
  
  static logIn = (values, history) => async dispatch => {
    try {
      let {data} = await APIs.UserApi.logIn(values);
      LocalStorage.setAuthToken(data.token);
      dispatch(UserActions.getCurrentUserInfo());
      dispatch(ModalFormActions.closeModalForm());
      history.goBack();
    } catch (e) {
      console.log(e);
      toast.error(e.message);
    }
  };
  
  static signUp = (values, history) => async dispatch => {
    try {
      await APIs.UserApi.signUp(values);
      dispatch(ModalFormActions.closeModalForm());
      history.push('/?modal=login');
    } catch (e) {
      console.log(e);
      toast.error(e.message);
    }
  };
  
  static  updateUserInfo = async (values, infoType) => {
    await APIs.UserApi.updateUserInfo(values, infoType);
  };
  
}

export const  authUser = payload => ({
  type:    USER_ACTIONS.AUTH_USER,
  payload,
});

export const purgeUser = () => {
  LocalStorage.setAuthToken('');
  return {
    type: USER_ACTIONS.PURGE_USER,
    payload: []
  };
};

export const getCurrentUserInfo = () => async dispatch => {
  try {
    let response = await APIs.UserApi.getCurrentUserInfo();
    dispatch(UserActions.authUser(response.data));
  } catch (e) {
    if (e.status === 401) {
      dispatch(UserActions.purgeUser());
    }
  }
};

export const logIn = (values, history) => async dispatch => {
  try {
    let {data} = await APIs.UserApi.logIn(values);
    LocalStorage.setAuthToken(data.token);
    dispatch(UserActions.getCurrentUserInfo());
    dispatch(ModalFormActions.closeModalForm());
    history.goBack();
  } catch (e) {
    console.log(e);
    toast.error(e.message);
  }
};

export const signUp = (values, history) => async dispatch => {
  try {
    await APIs.UserApi.signUp(values);
    dispatch(ModalFormActions.closeModalForm());
    history.push('/?modal=login');
  } catch (e) {
    console.log(e);
    toast.error(e.message);
  }
};

export const  updateUserInfo = async (values, infoType) => {
  await APIs.UserApi.updateUserInfo(values, infoType);
};