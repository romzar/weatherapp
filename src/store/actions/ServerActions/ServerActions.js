import { SERVER_ACTIONS } from './constants';

export class ServerActions {
  static setApiEnabled  = () => ({
    type: SERVER_ACTIONS.ENABLE_API,
  });
  static setApiDisabled = () => ({
    type: SERVER_ACTIONS.DISABLE_API,
  });
}