import { toast }           from 'react-toastify';
import { APIs }            from '../../../lib/Api';
import { WeatherActions }  from '../WeatherActions';
import { COUNTRY_ACTIONS } from './constants';

export class CountryActions {
  static getCountryInfoStart = () => {
    return {
      type: COUNTRY_ACTIONS.GET_COUNTRY_INFO_START,
    };
  };
  
  static getCountryInfoSuccess = response => {
    return {
      type:    COUNTRY_ACTIONS.GET_COUNTRY_INFO_SUCCESS,
      payload: response,
    };
  };
  
  static getCountryInfoFail = () => {
    return {
      type: COUNTRY_ACTIONS.GET_COUNTRY_INFO_FAIL,
    };
  };
  
  static fetchCurrentCountryInfo = () => {
    return async dispatch => {
      const {getCountryInfoStart, getCountryInfoSuccess, getCountryInfoFail} = CountryActions;
      
      dispatch(WeatherActions.getForecastStart(''));
      dispatch(getCountryInfoStart());
      
      try {
        let {CurrentLocationApi, CountriesApi} = APIs;
        const location                         = await CurrentLocationApi.getCurrentLocationInfo();
        const countryInfo                      = await CountriesApi.getCountryInfoByCode(location.country);
        dispatch(getCountryInfoSuccess(countryInfo));
        
        return countryInfo;
      } catch (e) {
        console.log(e.message);
        toast.error('Error while fetching country info');
        dispatch(getCountryInfoFail());
        dispatch(WeatherActions.getForecastFail());
      }
    };
  };
  static fetchCountryInfo        = (country) => {
    return async dispatch => {
      const {getCountryInfoStart, getCountryInfoSuccess, getCountryInfoFail} = CountryActions;
      dispatch(WeatherActions.getForecastStart(''));
      dispatch(getCountryInfoStart());
      try {
        let {CountriesApi} = APIs;
        const countryInfo  = await CountriesApi.getCountryInfoByName(country);
        dispatch(getCountryInfoSuccess(countryInfo));
        return countryInfo;
      } catch (e) {
        console.log(e.message);
        toast.error('Error while fetching country info');
        dispatch(getCountryInfoFail());
        dispatch(WeatherActions.getForecastFail());
      }
    };
  };
}