export const COUNTRY_ACTIONS = {
  GET_COUNTRY_INFO_START:   'GET_COUNTRY_INFO_START',
  GET_COUNTRY_INFO_SUCCESS: 'GET_COUNTRY_INFO_SUCCESS',
  GET_COUNTRY_INFO_FAIL:    'GET_COUNTRY_INFO_FAIL',
};
