import { applyMiddleware, createStore, compose } from 'redux';
import { persistStore }                 from "redux-persist";
import thunkMiddleware                  from 'redux-thunk';
import { rootReducer }                  from './reducers';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(
  rootReducer,
  composeEnhancers(
  applyMiddleware(thunkMiddleware))
);

export const persistor = persistStore(store);

