import { COUNTRY_ACTIONS } from '../actions/CountryActions';

let initialState = {
  isFetching: true,
  isError:    false,
  country:    {
    name:       '',
    flag:       '',
    currencies: [{name: '', symbol: ''}],
    languages:  [{name: ''}],
    population: '',
    region:     '',
  },
};

export const countryReducer = (state = initialState, action) => {
  return {
    [action.type]:                              () => (state),
    [COUNTRY_ACTIONS.GET_COUNTRY_INFO_START]:   () => ({
      ...state,
      isFetching: true,
      isError:    false,
    }),
    [COUNTRY_ACTIONS.GET_COUNTRY_INFO_SUCCESS]: () => ({
      isFetching: false,
      isError:    false,
      country:    action.payload,
    }),
    [COUNTRY_ACTIONS.GET_COUNTRY_INFO_FAIL]:    () => ({
      ...initialState,
      isError: true,
    }),
    
  }[action.type]();
};


