import { SERVER_ACTIONS } from '../actions/ServerActions/constants';

let initialState = {
  isApiEnabled: true,
};

export function serverReducer (state = initialState, action) {
  return {
    [action.type]:                () => state,
    [SERVER_ACTIONS.ENABLE_API]:  () => ({
      isApiEnabled: true,
    }),
    [SERVER_ACTIONS.DISABLE_API]: () => ({
      isApiEnabled: false,
    }),
  }[action.type]();
}