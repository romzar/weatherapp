import { MODAL_FORM_ACTIONS } from '../actions/ModalFormActions';

const initialState = {
  isModalOpen: false,
  type:        '',
};

export const modalFormReducer = (state = initialState, action) => {
  return {
    [action.type]:                         () => state,
    [MODAL_FORM_ACTIONS.OPEN_LOGIN_FORM]:  () => ({
      isModalOpen: true,
      type:        MODAL_FORM_ACTIONS.OPEN_LOGIN_FORM,
    }),
    [MODAL_FORM_ACTIONS.OPEN_SIGNUP_FORM]: () => ({
      isModalOpen: true,
      type:        MODAL_FORM_ACTIONS.OPEN_SIGNUP_FORM,
    }),
    [MODAL_FORM_ACTIONS.CLOSE_MODAL_FORM]: () => ({
      isModalOpen: false,
    }),
  }[action.type]();
};