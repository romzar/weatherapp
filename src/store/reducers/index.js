import { combineReducers }  from 'redux';
import { countryReducer }   from './CountryReducer';
import { modalFormReducer } from './ModalFormReducer';
import { serverReducer }    from './ServerReducer';
import { userReducer }      from './UserReducer';
import { weatherReducer }   from './WeatherReducer';


import { persistStore, persistReducer } from 'redux-persist'
import storage                          from 'redux-persist/lib/storage'


const persistConfig = {
  key: 'persistedUser',
  storage,
};

const persistedUserReducer = persistReducer(persistConfig, userReducer);



export const rootReducer = combineReducers({
  country: countryReducer,
  weather: weatherReducer,
  modal:   modalFormReducer,
  user:    persistedUserReducer,
  server:  serverReducer,
});

