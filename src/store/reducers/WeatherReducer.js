import { WEATHER_ACTIONS } from '../actions/WeatherActions/';

let initialState = {
  isFetching: true,
  isError:    false,
  forecast:   {
    title:                '',
    time:                 '',
    consolidated_weather: [],
  },
};

export function weatherReducer (state = initialState, action) {
  return {
    [action.type]:                          () => (state),
    [WEATHER_ACTIONS.GET_FORECAST_START]:   () => ({
      ...state,
      isFetching: true,
      isError:    false,
    }),
    [WEATHER_ACTIONS.GET_FORECAST_SUCCESS]: () => ({
      isFetching: false,
      isError:    false,
      forecast:   action.payload,
    }),
    [WEATHER_ACTIONS.GET_FORECAST_FAIL]:    () => ({
      ...initialState,
      isError: true,
    }),
    
  }[action.type]();
}
