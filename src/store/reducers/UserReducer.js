import { USER_ACTIONS }   from '../actions/UserActions/constants';

let initialState = {
  isAuthorized: false,
  
  profile:
    {
      firstName: ' ',
      lastName:  ' ',
      logo:      {},
      contacts:  {},
      address:   {},
    },
  
};

export function userReducer (state = initialState, action) {
  return {
    [action.type]:                () => state,
    [USER_ACTIONS.AUTH_USER]:     () => ({
      isAuthorized: true,
      ...action.payload,
    }),
    [USER_ACTIONS.PURGE_USER]:    () => ({
      ...initialState,
    }),
  }[action.type]();
}