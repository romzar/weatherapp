import React, { useEffect }         from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter }               from 'react-router-dom';
import { AddressForm }              from '../../components/forms/UserForms/AddressForm/AddressForm';
import { ContactsForm }             from '../../components/forms/UserForms/ContactsForm';
import { ProfileForm }              from '../../components/forms/UserForms/ProfileForm';
import { UserActions }              from '../../store/actions/UserActions';

const UserInfoPage = props => {
  const user     = useSelector(store => store.user);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(UserActions.getCurrentUserInfo());
  }, [user.id]);
  
  return (
    <div className={'user-info_main main'}>
      <ProfileForm user={user}/>
      <ContactsForm contacts={user.profile.contacts}/>
      <AddressForm address={user.profile.address}/>
    </div>
  );
};

export default withRouter(UserInfoPage);