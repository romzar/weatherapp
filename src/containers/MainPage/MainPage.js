import React       from 'react';
import { connect } from 'react-redux';

import { withRouter }                                from 'react-router-dom';
import { ToastContainer }                            from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { bindActionCreators }                        from 'redux';
import { CountryInfo }                               from '../../components/CountryInfo/CountryInfo';
import { Modal }                                     from '../../components/Modal';
import { WeatherInfo }                               from '../../components/WeatherInfo';
import { MAP_STATE_TO_MODAL, mapLocationToDispatch } from '../../lib/modals';
import * as ModalFormActions                           from '../../store/actions/ModalFormActions';
import  * as UserActions                                from '../../store/actions/UserActions';

class MainPage extends React.Component {
  
  componentDidMount = async () => {
    
    const {location, ModalFormActions} = this.props;
    mapLocationToDispatch(location, ModalFormActions);
  };
  
  componentDidUpdate = ({location: {search}}) => {
    const {location, ModalFormActions} = this.props;
    if (search !== location.search) {
      mapLocationToDispatch(location, ModalFormActions);
    }
  };
  
  render () {
    let {
          selectedCountry,
          weather,
          modal,
          ModalFormActions,
          history,
          UserActions,
        } = this.props;
    // console.log(UserActions);
    return (
      <>
        <div className="main">
          <CountryInfo value={selectedCountry}
                       isError={selectedCountry.isError}/>
          <WeatherInfo value={weather} isError={weather.isError}/>
        </div>
        <Modal
          handleClose={() => {
            ModalFormActions.closeModalForm();
            this.props.history.push('/');
          }}
          isOpen={modal.isModalOpen}
          component={MAP_STATE_TO_MODAL(modal.type)({
            history,
            UserActions,
          })}
        />
        <ToastContainer/>
      </>
    );
  }
}

const mapStateToProps    = state => {
  const {country, weather, modal, user} = state;
  return {
    selectedCountry: country,
    weather:         weather,
    modal:           modal,
    user:            user,
  };
};
const mapDispatchToProps = dispatch => ({
  ModalFormActions: bindActionCreators(ModalFormActions, dispatch),
  UserActions:      bindActionCreators(UserActions, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainPage));