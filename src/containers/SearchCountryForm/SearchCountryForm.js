import _                   from 'lodash';
import React, { useState } from 'react';
import { ComboBox }        from '../../components/common/InputControls/ComboBox';
import { SETTING }         from '../../configs';
import { APIs }            from '../../lib/Api';

export const SearchCountryForm = props => {
  
  const [isFetching, setFetchingState] = useState(false);
  const [values, setValues]            = useState([]);
  const [isError, setError]            = useState(false);
  
  const handleInput = (value) => {
    console.log(value);
    if (value && value.length >= SETTING.MIN_SUGGESTION_LENGTH) {
      setError(false);
      setFetchingState(true);
      
      getCountry(value).then((res) => {
        setValues(res);
        setFetchingState(false);
      }).catch((e) => {
        setFetchingState(false);
        setError(true);
      });
      
    }
  };
  
  const getCountry = (value) => {
    let api = APIs.CountriesApi;
    return api.getCountriesSuggestions(value);
  };
  
  const handleSuggestionClick = (value) => {
    //let {values} = this.state;
    props.onClick(value);
    setError(false);
    setFetchingState(false);
    setValues([]);
  };
  
  let {value} = props;
  
  return (
    <div className={'search-country-form'}>

      
      <ComboBox
        type={'comboBox'}
        suggestedValue={value||''}
        name="country"
        onInputChange={handleInput}
        options={values}
        isFetching={isFetching}
        onClick={handleSuggestionClick}
      />
    </div>
  );
};
