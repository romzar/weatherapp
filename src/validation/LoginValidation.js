import * as Yup from 'yup';

export const LOGIN_FORM = () => (
  Yup.object().shape({
    email:    Yup.string().required('Email is required'),//.email("Please enter a valid email"),
    password: Yup.string().required('Password is required'),
  })
);
