import * as Yup       from 'yup';
import { VALIDATION } from './constants';

const {
        MIN_FNAME_LENGTH,
        MAX_FNAME_LENGTH,
        MIN_LNAME_LENGTH,
        MAX_LNAME_LENGTH,
        MAX_ABOUTME_LENGTH,
      } = VALIDATION;

export const PROFILE_VALIDATION = Yup.object().shape({
  firstName: Yup.string().required('First name is required')
               .min(MIN_FNAME_LENGTH, 'First name must contain at least 2 letters')
               .max(MAX_FNAME_LENGTH, 'First name must contain maximum 20 letters'),
  lastName:  Yup.string().required('Last name is required')
               .min(MIN_LNAME_LENGTH, 'Last name must contain at least 2 letters')
               .max(MAX_LNAME_LENGTH, 'Last name must contain maximum 20 letters'),
  aboutMe:   Yup.string()
               .max(MAX_ABOUTME_LENGTH, 'About me section must contain maximum 100 symbols'),
  // birthday:  Yup.object().nullable().test(
  //   'birthday date validation',
  //   'Date is invalid',
  //   value => {
  //     console.log(value);
  //     // console.log(moment(value));
  //
  //     return value ? value._isValid : true;
  //   },
  // ),
});