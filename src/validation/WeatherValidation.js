import _                  from 'lodash';
import * as Yup           from 'yup';
import { WEATHER_STATES } from '../components/forms/WeatherForm/constants';

const WeatherShape = (fields) => {
  let res = {};
  _.keys(fields).forEach((id) => {
    res[id] = Yup.object().shape({
      the_temp:           Yup.number().typeError('Temperature must be a number')
                            .required('Temperature is required')
                            .when([`weather_state_abbr`], (selectvalue, schema) => {
                              let {minTemp, maxTemp} = WEATHER_STATES[selectvalue];
                              return schema.min(minTemp, `Temperature must be higher than ${minTemp} `)
                                .max(maxTemp, `Temperature must be less than ${maxTemp} `);
                            }),
      weather_state_abbr: Yup.string().oneOf(
        _.keys(WEATHER_STATES),
      ),
    });
  });
  return res;
};

export const WEATHER_FORM = (fields) => {
  return Yup.object().shape(
    WeatherShape(fields),
  );
};