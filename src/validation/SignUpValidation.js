import { parsePhoneNumberFromString } from 'libphonenumber-js';
import * as Yup                       from 'yup';
import { VALIDATION }                 from './constants';

const {
                      MIN_FNAME_LENGTH,
                      MAX_FNAME_LENGTH,
                      MIN_LNAME_LENGTH,
                      MAX_LNAME_LENGTH,
                      MIN_PASSWORD_LENGTH,
                      MAX_PASSWORD_LENGTH,
      } = VALIDATION;

export const SIGN_UP_FORM = () => (
  Yup.object().shape({
    email:           Yup.string().email('Please enter a valid email')
                       .required('Email is required'),
    password:        Yup.string()
                       .required('Password is required')
                       .min(MIN_PASSWORD_LENGTH, 'Password must contain at least 8 symbols')
                       .max(MAX_PASSWORD_LENGTH, 'Password must contain maximum 20 symbols'),
    confirmPassword: Yup.string().required('Please confirm your password')
                       .oneOf(
                         [Yup.ref('password')],
                         'Passwords doesn\'t match',
                       ),
    firstName:       Yup.string().required('First name is required')
                       .min(MIN_FNAME_LENGTH, 'First name must contain at least 2 letters')
                       .max(MAX_FNAME_LENGTH, 'First name must contain maximum 20 letters'),
    lastName:        Yup.string().required('Last name is required')
                       .min(MIN_LNAME_LENGTH, 'Last name must contain at least 2 letters')
                       .max(MAX_LNAME_LENGTH, 'Last name must contain maximum 20 letters'),
    phone:           Yup.string()
                       .required('Phone number is required')
                       .test(
                         'test-phone',
                         'Phone number is invalid',
                         value => value && parsePhoneNumberFromString(value) ?
                           parsePhoneNumberFromString(value).isValid() :
                           false,
                       ),
    subscribe:       Yup.bool(),
  })
);