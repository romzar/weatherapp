import { parsePhoneNumberFromString } from 'libphonenumber-js';
import * as Yup                       from 'yup';

export const CONTACTS_VALIDATION = Yup.object().shape({
  email:   Yup.string().email('Please enter a valid email'),
  website: Yup.string().url('Please enter a valid url'),
  skype:   Yup.string(),
  phone:   Yup.string().test(
    'test-phone',
    'Phone number is invalid',
    value => (value
      ? parsePhoneNumberFromString(value).isValid()
      : false),
  ),
});