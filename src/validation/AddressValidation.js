import isPostalCode from 'validator/es/lib/isPostalCode';
import * as Yup     from 'yup';
import { SETTING }  from '../configs';

export const ADDRESS_VALIDATION = Yup.object().shape({
  country:     Yup.string().min(2, 'Country name must contain at least 2 characters').max(64, 'Country name must contain maximum 64 characters'),
  postalCode:  Yup.string().test('test-postalCode',
    'Input valid postal code',
    value => value && isPostalCode(value, SETTING.DEFAULT_LOCALE.toUpperCase())),
  addressLine: Yup.string().min(2, 'Address line must contain at least 2 characters').max(256, 'Address line must contain maximum 256 characters'),
});