import moment             from 'moment';
import React              from 'react';
import { useTranslation } from 'react-i18next';
import { CustomLoader }   from '../CustomLoader';
import { WeatherForm }    from '../forms/WeatherForm';

export const WeatherInfo = props => {
  let {t}                                      = useTranslation();
  let {isError, value: {forecast, isFetching}} = props;
  
  let formattedTime = moment(forecast.time)
    .utcOffset(forecast.time)
    .format('HH:mm');
  
  return (
    <div className="weather-info">
      {!isError
        ? (
          !isFetching
            ? (
              <>
                <h1>
                  {t('Weather in', {
                    city: forecast.title,
                    time: formattedTime,
                  })}
                </h1>
                <WeatherForm values={forecast}/>
              </>
            )
            : <CustomLoader/>
        )
        : null
      }
    
    </div>
  );
};