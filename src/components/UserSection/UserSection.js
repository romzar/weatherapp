import { Avatar }         from '@material-ui/core';
import ExitToAppIcon      from '@material-ui/icons/ExitToApp';
import React              from 'react';
import { useTranslation } from 'react-i18next';
import { Link }           from 'react-router-dom';

export const UserSection = props => {
  let {firstName, lastName, logo, isAuthorized} = props;
  
  const {i18n} = useTranslation();
  return (
    isAuthorized
      ? (
        <div className={'user-section'}>
          {
            !!logo
              ? <Avatar src={logo.location}/>
              : <Avatar>{firstName[0]}</Avatar>
          }
          <span className={'name'}>
            <Link
              to={`/${i18n.language}/user`}>{`${firstName} ${lastName}`}
            </Link>
          </span>
          <button className={'logout-btn'}
                  onClick={(e) => {
                    e.preventDefault();
                    props.purgeUser();
                  }}>
            <ExitToAppIcon/>
          </button>
        </div>
      )
      : null
  );
};