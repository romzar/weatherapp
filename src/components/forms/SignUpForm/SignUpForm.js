import Button             from '@material-ui/core/Button';
import FormControlLabel   from '@material-ui/core/FormControlLabel';
import Grid               from '@material-ui/core/Grid';
import Switch             from '@material-ui/core/Switch';
import TextField          from '@material-ui/core/TextField';
import Typography         from '@material-ui/core/Typography';
import { Formik }         from 'formik';
import React              from 'react';
import { Link }           from 'react-router-dom';
import { useImageUpload } from '../../../lib/hooks/useImageUpload';
import { SIGN_UP_FORM }   from '../../../validation';

const SignUpFormBody = (formik) => {
  let {handleSubmit, handleChange, handleBlur, errors, touched, setFieldValue} = formik;
  const uploadImage                                                            = useImageUpload();
  
  return (
    <div className={'paper'}>
      <Typography component="h1" variant="h5">
        Sign up
      </Typography>
      <form className={'modal-form sign-up-form'} onSubmit={handleSubmit}
            noValidate>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <TextField
              error={errors.firstName && touched.firstName}
              helperText={(touched.firstName && errors.firstName) || ' '}
              onChange={handleChange}
              onBlur={handleBlur}
              
              autoComplete="fname"
              name="firstName"
              variant="outlined"
              required
              fullWidth
              id="firstName"
              label="First Name"
              autoFocus
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              error={errors.lastName && touched.lastName}
              helperText={(touched.lastName && errors.lastName) || ' '}
              onChange={handleChange}
              onBlur={handleBlur}
              
              variant="outlined"
              required
              fullWidth
              id="lastName"
              label="Last Name"
              name="lastName"
              autoComplete="lname"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              error={errors.email && touched.email}
              helperText={(touched.email && errors.email) || ' '}
              onChange={handleChange}
              onBlur={handleBlur}
              
              variant="outlined"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              error={errors.password && touched.password}
              helperText={(touched.password && errors.password) || ' '}
              onChange={handleChange}
              onBlur={handleBlur}
              
              variant="outlined"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              error={errors.confirmPassword && touched.confirmPassword}
              helperText={(touched.confirmPassword && errors.confirmPassword) || ' '}
              onChange={handleChange}
              onBlur={handleBlur}
              
              variant="outlined"
              required
              fullWidth
              name="confirmPassword"
              label="Confirm password"
              type="password"
              id="confirmPassword"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              error={errors.phone && touched.phone}
              helperText={(touched.phone && errors.phone) || ' '}
              onChange={handleChange}
              onBlur={handleBlur}
              
              variant="outlined"
              required
              fullWidth
              name="phone"
              label="Phone"
              type="phone"
              id="phone"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                uploadImage(file => setFieldValue('logo', file));
              }}
            >
              Upload your photo
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            {
              formik.values.logo.name ? <img className={'logo-preview'} alt={''}
                                             src={window.URL.createObjectURL(formik.values.logo)}/> : null
            }
          </Grid>
          <Grid item xs={12}>
            <FormControlLabel
              control={<Switch onChange={handleChange} value="true"/>}
              id="subscribe" name="subscribe"
              label="I want to receive inspiration, marketing promotions and updates via email."
            />
          </Grid>
        </Grid>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={'submit-btn'}
        >
          Sign Up
        </Button>
        <Grid container justify="flex-end">
          <Grid item>
            <Link to="/?modal=login" variant="body2">
              Already have an account? Sign in
            </Link>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export const SignUpForm = (props) => {
  let {UserActions: {signUp}, history} = props;
  return (
    <Formik
      enableReinitialize
      initialValues={
        {
          email:           '',
          password:        '',
          confirmPassword: '',
          firstName:       '',
          lastName:        '',
          phone:           '',
          logo:            {},
          subscribe:       false,
        }
      }
      validationSchema={SIGN_UP_FORM()}
      onSubmit={
        values => {
          signUp(values, history);
        }
      }
    >
      {SignUpFormBody}
    </Formik>
  );
};
