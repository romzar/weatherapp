import { Formik }                     from 'formik';
import _                              from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation }             from 'react-i18next';
import { LocalStorage }               from '../../../lib/LocalStorage';
import { WEATHER_FORM }               from '../../../validation';
import { FormItem }                   from './FormItem';

export const WeatherForm = (props) => {
  
  const [isDisabled, toggleDisabled]             = useState(true);
  const {values: {consolidated_weather, parent}} = props;
  
  const [fieldsData, setFieldsData] = useState({});
  let {t}                           = useTranslation();
  const mergeWeatherData            = (weatherArr, dataSource = {}) => {
    let temp = {};
    weatherArr.forEach((item) => {
      dataSource    = dataSource || {};
      temp[item.id] = {
        the_temp:           _.get(dataSource, `${item.id}.the_temp`, item.the_temp),
        applicable_date:    item.applicable_date,
        weather_state_abbr: _.get(dataSource, `${item.id}.weather_state_abbr`, item.weather_state_abbr),
      };
    });
    setFieldsData(temp);
  };
  
  useEffect(() => {
    mergeWeatherData(consolidated_weather, LocalStorage.getChangedCountryWeather(parent.title));
  }, [parent.title, isDisabled, consolidated_weather]);
  
  return (
    <Formik
      enableReinitialize
      initialValues={
        fieldsData
      }
      validationSchema={
        WEATHER_FORM(fieldsData)
      }
      validateOnChange={false}
      validateOnBlur={false}
      validateOnSubmit={true}
      onSubmit={values => {
        toggleDisabled(!isDisabled);
        !_.isEqual(fieldsData, values) && LocalStorage.addChangedCountryWeather(parent.title, values);
      }}
    >
      {({handleSubmit, setFieldValue, values, errors, touched, resetForm}) => (
        <form onSubmit={handleSubmit}>
          {_.toPairs(values).map(([id, data], index) => {
            return (
              <FormItem key={index} value={{id, ...data}}
                        isDisabled={isDisabled}
                        touched={touched}
                        errors={errors}
                        onChange={
                          (e) => {
                            setFieldValue(`${id}.weather_state_abbr`, e.target.value);
                          }
                        }
                        onInput={e => {
                          setFieldValue(`${id}.the_temp`, e.target.value);
                        }}
              />
            
            );
          })}
          <button type="submit" disabled={isDisabled}>{t('Submit')}</button>
          <button
            disabled={isDisabled}
            onClick={e => {
              e.preventDefault();
              resetForm(fieldsData);
              toggleDisabled(!isDisabled);
            }}
          >
            {t('Cancel')}
          </button>
          <button
            onClick={(e) => {
              e.preventDefault();
              toggleDisabled(!isDisabled);
            }}
            disabled={!isDisabled}
          >
            {t('Edit')}
          </button>
        </form>
      )}
    </Formik>
  
  );
};