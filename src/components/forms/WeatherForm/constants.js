const MAX_TEMP = 70;
const MIN_TEMP = -90;

export const WEATHER_STATES = {
  sn: {
    name:    'Snow',
    minTemp: MIN_TEMP,
    maxTemp: 10,
  },
  sl: {
    name:    'Sleet',
    minTemp: -5,
    maxTemp: 10,
  },
  h:  {
    name:    'Hail',
    minTemp: -10,
    maxTemp: 20,
  },
  t:  {
    name:    'Thunderstorm',
    minTemp: -5,
    maxTemp: MAX_TEMP,
  },
  hr: {
    name:    'Heavy Rain',
    minTemp: -10,
    maxTemp: MAX_TEMP,
  },
  lr: {
    name:    'Light Rain',
    minTemp: -10,
    maxTemp: MAX_TEMP,
  },
  s:  {
    name:    'Showers',
    minTemp: -10,
    maxTemp: MAX_TEMP,
  },
  hc: {
    name:    'Heavy Cloud',
    minTemp: MIN_TEMP,
    maxTemp: MAX_TEMP,
  },
  lc: {
    name:    'Light Cloud',
    minTemp: MIN_TEMP,
    maxTemp: MAX_TEMP,
  },
  c:  {
    name:    'Clear',
    minTemp: MIN_TEMP,
    maxTemp: MAX_TEMP,
  },
  
};
