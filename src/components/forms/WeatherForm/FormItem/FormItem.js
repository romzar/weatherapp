import _                  from 'lodash';
import React              from 'react';
import { useTranslation } from 'react-i18next';
import { WEATHER_STATES } from '../constants';

export const FormItem = props => {
  const {value, onChange, isDisabled, onInput} = props;
  
  const inputValue = Math.floor(value.the_temp) || value.the_temp;
  
  const selectValue = value.weather_state_abbr;
  let {t} = useTranslation();
  return (
    <div className="row">
      <span>
        {value.applicable_date}
      </span>
      
      <input id={`${value.id}.the_temp`} name={`${value.id}.the_temp`}
             value={inputValue === ''?'':Math.floor(inputValue)}
             autoComplete="off"
             onInput={onInput}
             type={'number'}
             disabled={isDisabled}
      />
      {props.errors[value.id] && props.errors[value.id].the_temp ? (
        <div className={'error'}>{props.errors[value.id].the_temp}</div>
      ) : null}
      <select
        value={selectValue}
        id={`${value.id}.weather_state_abbr`}
        name={`${value.id}.weather_state_abbr`}
        disabled={isDisabled}
        onChange={onChange}>
        {
          _.toPairs(WEATHER_STATES).map(([abbr, state], index) => (
            <option value={abbr} key={index}>
              {t(state.name)}
            </option>
          ))
        }
      </select>
      <img alt="" width={40}
           src={`https://www.metaweather.com/static/img/weather/${selectValue}.svg`}/>
    </div>
  );
};