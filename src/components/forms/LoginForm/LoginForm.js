import Button           from '@material-ui/core/Button';
import Checkbox         from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid             from '@material-ui/core/Grid';
import TextField        from '@material-ui/core/TextField';
import Typography       from '@material-ui/core/Typography';
import { Formik }       from 'formik';
import React            from 'react';
import { Link }         from 'react-router-dom';
import { toast }        from 'react-toastify';
import { APIs }         from '../../../lib/Api';
import { LocalStorage } from '../../../lib/LocalStorage';
import { LOGIN_FORM }   from '../../../validation/LoginValidation';

const LoginFormBody = ({handleSubmit, handleChange, handleBlur, errors, touched}) => (
  <div className={'paper'}>
    
    <Typography component="h1" variant="h5">
      Sign in
    </Typography>
    <form className={'modal-form'} onSubmit={handleSubmit} noValidate>
      <TextField
        
        variant="outlined"
        margin="normal"
        error={errors.email && touched.email}
        helperText={(touched.email && errors.email) || ' '}
        onChange={handleChange}
        onBlur={handleBlur}
        required
        fullWidth
        id="email"
        label="Email Address"
        name="email"
        autoComplete="email"
        autoFocus
      />
      <TextField
        error={errors.password && touched.password}
        helperText={(touched.password && errors.password) || ' '}
        variant="outlined"
        margin="normal"
        onChange={handleChange}
        onBlur={handleBlur}
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
      />
      <FormControlLabel
        control={<Checkbox value="remember" color="primary"/>}
        label="Remember me"
      />
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        className={'submit-btn'}
      >
        Sign In
      </Button>
      <Grid container>
        <Grid item xs>
          <Link to="#" variant="body2">
            Forgot password?
          </Link>
        </Grid>
        <Grid item>
          <Link to={'/?modal=signup'} variant="body2">
            {'Don\'t have an account? Sign Up'}
          </Link>
        </Grid>
      </Grid>
    </form>
  </div>
);

export const LoginForm = (props) => {
  let {UserActions:{logIn}, history} = props;
  return (
    <Formik
      enableReinitialize
      initialValues={
        {
          email:    '',
          password: '',
        }
      }
      validationSchema={
        LOGIN_FORM()
      }
      onSubmit={
        async values => {
          console.log(props);
          logIn(values, history);
        }
      }
    >
      {LoginFormBody}
    </Formik>
  );
};
