import Button                         from '@material-ui/core/Button';
import ArrowDropDownIcon              from '@material-ui/icons/ArrowDropDown';
import { Formik }                     from 'formik';
import _                              from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation }             from 'react-i18next';
import { LocationActions }            from '../../../../store/actions/LocationActions/LocationActions';
import { UserActions }                from '../../../../store/actions/UserActions';
import { ADDRESS_VALIDATION }         from '../../../../validation/AddressValidation';
import { ComboBox }                   from '../../../common/InputControls/ComboBox';
import { Input }                      from '../../../common/InputControls/Input';
import { CustomSelect }               from '../../../common/InputControls/Select';

const FormBody = props => {
  let {
        // resetForm,
        errors,
        touched,
        handleChange,
        handleBlur,
        values,
        handleSubmit,
        // initialValues,
        setFieldValue,
        handleReset,
    isDisabled,
    toggleDisabled,
        // resetForm,
      }                                           = props;
  const [paginationOptions, setPaginationOptions] = useState({});
  const [regions, setRegions]                     = useState([]);
  const [cities, setCities]                       = useState([]);
  const [citiesFetching, setCitiesFetching]       = useState(false);
  const [comboInputVal, setComboInputVal]         = useState('');
  const [isRegionChosen, setRegionChosen]         = useState(false);
  const {t} = useTranslation();
  const onScroll = async () => {
    if (cities.length < paginationOptions.totalRecords) {
      console.log('Cities fetching');
      let result = await LocationActions.getCities(values.regionId, comboInputVal, cities.length);
      setPaginationOptions(result.options.pagination);
      setCities([...cities, ...result.data]);
    }
  };
  
  useEffect(() => {
    LocationActions.getRegions()
      .then(({data}) => setRegions(data));
    if (values.regionId) {
      setRegionChosen(true);
    }
  }, []);
  
  useEffect(() => {
    LocationActions.getCities(values.regionId, comboInputVal || values.city && values.city.name)
      .then(
        result => {
          setCities(result.data);
          setPaginationOptions(result.options.pagination);
        },
      );
    if (values.regionId !== null) {
      console.log('!null', isDisabled, isRegionChosen);
      setRegionChosen(true);
    } else {
      console.log('null', isDisabled, isRegionChosen);
      setRegionChosen(false);
    }
  }, [values.regionId]);

  const handleInputChange = async (value) => {
    setCitiesFetching(true);
    
    console.log(value);
    let result = await LocationActions.getCities(values.regionId, value);
    setCitiesFetching(false);
    setPaginationOptions(result.options.pagination);
    setCities(result.data);
    setComboInputVal(value);
  };
  
  return (
    <form onSubmit={handleSubmit} className={'address-form'}>
      <h2>{t('Address')}</h2>
      <Input
        type={'text'}
        name={'country'}
        value={values.country || ''}
        label={`${t('Country')}:`}
        error={!!errors.country}
        helperText={errors.country || ' '}
        onChange={handleChange}
        onBlur={handleBlur}
        disabled={isDisabled}
      />
      <CustomSelect
        IconComponent={!isDisabled ? ArrowDropDownIcon : () => null}
        noOptionsSelectedText={`${t('Choose region')}`}
        type={'select'}
        name={'regionId'}
        options={regions}
        value={values.regionId || ''}
        label={`${t('Region')}:`}
        error={!!errors.regionId}
        onChange={(e, o) => {
          
          if (o.props.value !== values.regionId) {
            setFieldValue('cityId', null);
            setFieldValue('city', {});
          }
          
          handleChange(e);
          
          if (o.props.value === '') {
            setFieldValue('regionId', null);
            setFieldValue('region', {});
            setRegionChosen(false);
          }
        }}
        onBlur={handleBlur}
        disabled={isDisabled}
      />
      
      <ComboBox
        inputName={'cityId'}
        suggestedValue={values.city && values.city.name}
        options={(values.regionId && cities) || []}
        label={`${t('City')}:`}
        onClick={value => {
          setFieldValue('cityId', value && value.id);
          setFieldValue('city', value);
        }}
        isFetching={citiesFetching}
        onScroll={onScroll}
        onInputChange={handleInputChange}
        noOptionsSelectedText={`${t('Choose city')}`}
        disabled={isDisabled || !isRegionChosen}
      />
      <Input
        type={'text'}
        name={'postalCode'}
        value={values.postalCode || ''}
        label={`${t('Postal code')}:`}
        error={!!errors.postalCode}
        helperText={errors.postalCode || ' '}
        onChange={handleChange}
        onBlur={handleBlur}
        disabled={isDisabled}
      />
      <Input
        type={'textarea'}
        name={'addressLine'}
        value={values.addressLine || ''}
        label={`${t('Address line')}:`}
        error={!!errors.addressLine}
        helperText={errors.addressLine || ' '}
        onChange={handleChange}
        onBlur={handleBlur}
        disabled={isDisabled}
      />
      <Button
        variant="contained"
        color="primary"
        disabled={!isDisabled}
        onClick={(e) => {
          toggleDisabled(!isDisabled);
          e.preventDefault();
        }}
      >
        {t('Edit')}
      </Button>
      <Button
        variant="contained"
        color="primary"
        type={'submit'}
        disabled={isDisabled}
      >
        {t('Submit')}
      </Button>
      <Button
        variant="contained"
        color="primary"
        disabled={isDisabled}
        onClick={(e) => {
          // setFieldValue('city', initialValues.city);
          // resetForm(initialValues);
          e.preventDefault();
          handleReset();
          toggleDisabled(!isDisabled);
          console.log(values);
        }}
      >
        {t('Cancel')}
      </Button>
    
    </form>
  );
};

export const AddressForm = props => {
  const [isDisabled, toggleDisabled]              = useState(true);
  
  let initialValues = {
    ...props.address,
  };
  console.log(initialValues);
  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validationSchema={
        ADDRESS_VALIDATION
      }
      onSubmit={
        values => {
          toggleDisabled(!isDisabled);
          if(_.isEqual(initialValues, values)) return;
          let result = _.omit(values, ['id', 'profileId', 'region', 'city']);
          UserActions.updateUserInfo(result, 'address');
        }
      }
    >{formik => <FormBody {...formik} initialValues={initialValues} isDisabled={isDisabled}
                          toggleDisabled={toggleDisabled}/>}</Formik>
  
  );
};