import React     from 'react';
import { Input } from '../../../common/InputControls/Input';

export const FormItem = props => {
  let {inputName, error, touched, label, handleChange, handleBlur, value, disabled, type, options, onInputChange, onClick, suggestedValue} = props;
  return (
    <div className={'form-item'}>
      {label && <span className={'label'}>
        {label}
      </span>}
      {
        <Input
          options={options}
          type={type}
          suggestedValue={suggestedValue}
          name={inputName}
          value={value}
          tooltip={error}
          onClick={onClick}
          onInputChange={onInputChange}
          showTooltip={touched && error}
          handleChange={handleChange}
          handleBlur={handleBlur}
          disabled={disabled}
        />
      }
    </div>
  );
};