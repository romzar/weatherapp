import Button                  from '@material-ui/core/Button';
import { Formik }              from 'formik';
import _                       from 'lodash';
import React, { useState }     from 'react';
import { useTranslation }      from 'react-i18next';
import { UserActions }         from '../../../../store/actions/UserActions';
import { CONTACTS_VALIDATION } from '../../../../validation';
import { Input }               from '../../../common/InputControls/Input';

const FormBody = props => {
  let {errors, touched, handleChange, handleBlur, values, handleSubmit, resetForm, initialValues, isDisabled, toggleDisabled} = props;
  const {t} = useTranslation();
  return (
    <form onSubmit={handleSubmit} className={'contacts-form'}>
      <h2>{t('Contacts')}</h2>
        <Input
          type={'text'}
          name={'email'}
          value={values.email || ' '}
          label={`${t('Email')}:`}
          helperText={errors.email || ' '}
          error={!!errors.email}
          touched={touched.email}
          onChange={handleChange}
          onBlur={handleBlur}
          disabled={isDisabled}
        />
        <Input
          type={'text'}
          name={'website'}
          value={values.website || ''}
          label={`${t('Website')}:`}
          helperText={errors.website || ' '}
          error={!!errors.website}
          onChange={handleChange}
          onBlur={handleBlur}
          disabled={isDisabled}
        />
        <Input
          type={'text'}
          name={'skype'}
          value={values.skype || ''}
          label={`${t('Skype')}:`}
          helperText={errors.skype || ' '}
          error={!!errors.skype}
          onChange={handleChange}
          onBlur={handleBlur}
          disabled={isDisabled}
        />
        <Input
          type={'text'}
          name={'phone'}
          value={values.phone || ' '}
          label={`${t('Phone')}:`}
          helperText={errors.phone || ' '}
          error={!!errors.phone}
          touched={touched.phone}
          onChange={handleChange}
          onBlur={handleBlur}
          disabled={isDisabled}
        />
      <Button
        variant="contained"
        color="primary"
        
        disabled={!isDisabled}
        onClick={() => {
          toggleDisabled(!isDisabled);
        }}
      >
        {t('Edit')}
      </Button>
      <Button
        variant="contained"
        color="primary"
        type={'submit'}
        disabled={isDisabled}
      >
        {t('Submit')}
      </Button>
      <Button
        variant="contained"
        color="primary"
        disabled={isDisabled}
        onClick={() => {
          resetForm(initialValues);
          toggleDisabled(!isDisabled);
        }}
      >
        {t('Cancel')}
      </Button>
    
    </form>
  );
};

export const ContactsForm = props => {
  
  const initialValues = {
    ...props.contacts,
  };
  
  const [isDisabled, toggleDisabled] = useState(true);
  
  return (
    <Formik
      enableReinitialize
      initialValues={
        initialValues
      }
      validationSchema={
        CONTACTS_VALIDATION
      }
      validateOnBlur={true}
      validateOnChange={false}
      
      onSubmit={
        values => {
          toggleDisabled(!isDisabled);
          console.log(values);
          if(_.isEqual(initialValues, values)) return;
          let result = _.omit(values, ['id', 'userId']);
          UserActions.updateUserInfo(result, 'contacts');
          
        }
      }
    >{formik => <FormBody {...formik} initialValues={initialValues}
                          isDisabled={isDisabled}
                          toggleDisabled={toggleDisabled}/>}
    </Formik>
  
  );
};