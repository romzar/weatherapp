import Button                 from '@material-ui/core/Button';
import { Formik }             from 'formik';
import _                      from 'lodash';
import moment                 from 'moment';
import React, { useState }    from 'react';
import { useTranslation }     from 'react-i18next';
import { toast }              from 'react-toastify';
import logo
                              from '../../../../assets/images/istockphoto-666542874-170667a.jpg';
import { APIs }               from '../../../../lib/Api';
import { useImageUpload }     from '../../../../lib/hooks';
import { UserActions }        from '../../../../store/actions/UserActions';
import { PROFILE_VALIDATION } from '../../../../validation';
import { DatePicker }         from '../../../common/InputControls/DatePicker';
import { Input }              from '../../../common/InputControls/Input';
import { CustomAvatar }       from '../../../CustomAvatar';

const FormBody = props => {
  let {errors, handleChange, handleBlur, values, setFieldValue, handleSubmit, resetForm, initialValues, isDisabled, toggleDisabled} = props;
  
  const uploadImage = useImageUpload();
  const {t}         = useTranslation();
  console.log(values);
  const [logoSrc, setLogoSrc] = useState(values.logoLocation || logo);
  
  return (
    <form onSubmit={handleSubmit} className={'profile-form'}>
      <div className='logo'>
        <CustomAvatar
          alt={`${values.firstName} ${values.lastName}`}
          src={logoSrc}
          onAdd={() => {
            uploadImage(async (file) => {
              setFieldValue('logo', file);
              setLogoSrc(window.URL.createObjectURL(file));
              try {
                await APIs.UserApi.updateUserInfo({logo: file}, 'profile');
              } catch (e) {
                toast.error(e.message);
              }
            });
            
          }}
          onDelete={async () => {
            setFieldValue('logo', null);
            setFieldValue('logoLocation', null);
            try {
              await APIs.UserApi.updateUserInfo({logo: 'delete'}, 'profile');
            } catch (e) {
              toast.error(e.message);
            }
          }}
        />
      
      </div>
      <div className={'form-controls'}>
        <Input
          helperText={errors.firstName || ' '}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={!!errors.firstName}
          type={'text'}
          name={'firstName'}
          value={values.firstName}
          label={`${t('First name')}:`}
          disabled={isDisabled}
        />
        <Input
          type={'text'}
          name={'lastName'}
          value={values.lastName}
          label={`${t('Last name')}:`}
          error={!!errors.lastName}
          helperText={errors.lastName || ' '}
          handleChange={handleChange}
          handleBlur={handleBlur}
          disabled={isDisabled}
        />
        <DatePicker
          variant="inline"
          format="DD/MM/YYYY"
          margin="normal"
          id="birthday"
          name={'birthday'}
          label={`${t('Birthday')}:`}
          value={values.birthday}
          autoOk
          errors={errors}
          onChange={val => setFieldValue('birthday', val)}
          disableFuture
          minDate={moment('01/01/1900', 'DD/MM/YYYY')}
          minDateMessage={
            'Date of birth should be later than 01/01/1900'
          }
          maxDateMessage={
            'You cannot set future date as date of birth'
          }
          invalidDateMessage={
            'Input valid date in "DD/MM/YYYY" format'
          }
          disabled={isDisabled}
        />
        <Input
          type={'textarea'}
          className={'textarea'}
          name={'aboutMe'}
          value={values.aboutMe}
          helperText={errors.aboutMe || ' '}
          error={!!errors.aboutMe}
          label={`${t('About me')}:`}
          handleChange={handleChange}
          handleBlur={handleBlur}
          disabled={isDisabled}
        />
        <Button
          variant="contained"
          color="primary"
          disabled={!isDisabled}
          onClick={() => {
            toggleDisabled(!isDisabled);
          }}
        >
          {t('Edit')}
        </Button>
        <Button
          variant="contained"
          color="primary"
          type={'submit'}
          disabled={isDisabled}
        >
          {t('Submit')}
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={isDisabled}
          onClick={(e) => {
            resetForm(initialValues);
            toggleDisabled(!isDisabled);
          }}
        >
          {t('Cancel')}
        </Button>
      </div>
    </form>
  );
};

export const ProfileForm = props => {
  const {user: {profile}}            = props;
  const initialValues                = {
    ...profile,
    logoLocation: _.get(profile, 'logo.location', undefined),
    logo:         undefined,
  };
  const [isDisabled, toggleDisabled] = useState(true);
  
  return (
    <Formik
      enableReinitialize
      initialValues={
        initialValues
      }
      validationSchema={
        PROFILE_VALIDATION
      }
      validateOnBlur={true}
      validateOnChange={false}
      onSubmit={
        values => {
          toggleDisabled(!isDisabled);
          if (_.isEqual(initialValues, values)) return;
          let result = _.omit(values, ['id', 'logoLocation', 'address', 'contacts', 'logoId', 'userId', 'disableReason']);
          UserActions.updateUserInfo(result, 'profile');
          console.log('submit', values);
        }
      }
    >{formik => <FormBody {...formik} initialValues={initialValues}
                          toggleDisabled={toggleDisabled}
                          isDisabled={isDisabled}/>}</Formik>
  
  );
};