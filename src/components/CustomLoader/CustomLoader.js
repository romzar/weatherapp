import React from 'react';

export const CustomLoader = () => {
  
  return (
    <div className={'loader'}>
      <div className={'bounce1'}></div>
      <div className={'bounce2'}></div>
      <div className={'bounce3'}></div>
    </div>
  )
}