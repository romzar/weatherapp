import MenuItem from '@material-ui/core/MenuItem';
// import FormHelperText from '@material-ui/core/FormHelperText';
// import FormControl from '@material-ui/core/FormControl';
import Select   from '@material-ui/core/Select';
import React    from 'react';

export const CustomSelect = props => {
  let {variant, options, touched, error, label,noOptionsSelectedText, ...rest} = props;
  return (
    <div className={'input-container'}>
      {label && <span>{label}</span>}
      <Select
        variant={variant || 'standard'}
        displayEmpty={true}
        className={'select'}
        {...rest}
      >
        {noOptionsSelectedText&&<MenuItem value={''}>{noOptionsSelectedText}</MenuItem>}
        {
          options.map(option => (
            <MenuItem value={option.id} key={option.id}>
              {option.name}
            </MenuItem>
          ))
        }
      </Select>
    </div>
  );
};