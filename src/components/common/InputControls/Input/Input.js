import MomentUtils from '@date-io/moment';
import TextField   from '@material-ui/core/TextField';
import {
  KeyboardDatePicker, MuiPickersUtilsProvider,
}                  from '@material-ui/pickers';

import React, { useEffect, useState } from 'react';

export const Input = props => {
  
  const {
          name,
          suggestedValue,
          tooltip,
          showTooltip,
          handleChange,
          handleBlur,
          value,
          disabled,
          options,
          type,
          label,
          onClick,
          variant,
          isFetching,
          
          ...rest
        } = props;
  
  const mapTypeToComponent = (type) => {
    return {
      'text':
        () => (
          <TextField
            className={'input'}
            name={name} onChange={handleChange}
            onBlur={handleBlur}
            variant={variant || 'standard'}
            type="text"
            value={value}
            autoComplete="off" disabled={disabled}
            {...rest}
          />),
      
      'textarea':
              () => (
                <TextField
                  className={'input textarea'}
                  name={name} onChange={handleChange}
                  onBlur={handleBlur}
                  variant={variant || 'outlined'}
                  type="text"
                  multiline
                  rows={5}
                  value={value}
                  autoComplete="off" disabled={disabled}
                  {...rest}
                />),
    }[type]();
  };
  
  return (<div className={'input-container'}>
      {
        label && <span>{label}</span>
      }
      
      {
        mapTypeToComponent(type)
      }
      
      {
        showTooltip
          ? <span className={'tooltip'}>
                {tooltip}
              </span>
          : null
      }
    </div>
  );
};
