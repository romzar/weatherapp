import MomentUtils         from '@date-io/moment';
import {
  KeyboardDatePicker, MuiPickersUtilsProvider,
}                          from '@material-ui/pickers';
import _                   from 'lodash';
import React, { useState } from 'react';

export const DatePicker = props => {
  const [err, setErr]               = useState('');
  const [isErrHidden, setErrHidden] = useState(false);
  
  const {
          variant,
          value,
          label,
          onChange,
          required,
          errors,
          format,
          touched,
          name,
          maskChar,
          ...rest
        } = props;
  
  const valid = (val) => {
    let _err = `Input valid date in format ${format}`;
    
    if (_.isNil(val))
      required ? _setError(_err) : _purgeError();
    
    if (val) {
      let isValid  = val.isValid();
      let isHidden = !isValid && val._i && val._i.match(/\d/g).length !== 8;
      isValid ? _purgeError() : _setError(_err, isHidden);
    }
    
    return val;
  };
  
  const _purgeError = () => {
    setErr('');
    delete errors[name];
    return {success: true};
  };
  
  const _setError = (_err, hidden = false) => {
    setErr(_err);
    setErrHidden(hidden);
    errors[name] = _err;
    return {success: true};
  };
  
  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <div className={'input-container'}>
        {
          label && <span>{label}</span>
        }
        <KeyboardDatePicker
          className={'input'}
          variant={variant || 'inline'}
          value={value}
          format={format}
          helperText={(!isErrHidden && err) || ' '}
          error={!isErrHidden && !!err}
          onError={err => {
            if (err) {
              setErr(err);
              errors[name] = err;
            }
          }}
          onChange={val => {
            onChange(valid(val));
          }}
          onBlur={() => setErrHidden(false)}
          
          {...rest}
        />
      </div>
    </MuiPickersUtilsProvider>
  );
  
};