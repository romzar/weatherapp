import { MenuItem } from '@material-ui/core';
import TextField    from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React, {
  useEffect, useRef, useState,
}                   from 'react';
import {
  useKeyControl, useOffsetPagination, useOutsideClickHandler,
}                   from '../../../../lib/hooks';

export const ComboBox = props => {
  
  const popupRef = useRef(null);
  const inputRef = useRef(null);
  
  const resetCombobox = () => {
    setPopupShow(false);
    setSelected(0);
    setValue(suggestedValue || noOptionsSelectedText);
  };
  
  useOutsideClickHandler([popupRef, inputRef], resetCombobox);
  
  const [selected, setSelected] = useState(0);
  
  const [isPopupShown, setPopupShow] = useState(false);
  
  let {
        options,
        isFetching,
        onClick,
        variant,
        suggestedValue,
        onScroll,
        onInputChange,
        inputName,
        touched,
        isError,
        label,
        disabled,
        noOptionsSelectedText,
        ...rest
      } = props;
  
  
  
  let _options;
  if(noOptionsSelectedText){
    _options= [{name: noOptionsSelectedText}, ...options]
  } else {
    _options=[...options]
  }
  
  const [valueFromState, setValue] = useState(suggestedValue || noOptionsSelectedText||'');
  
  let {scrollTop, setScrollTop, scrollHandler} = useOffsetPagination(onScroll);
  
  const handleSuggestionClick = (item) => {
    
    setValue(suggestedValue || '');
    setPopupShow(false);
    onClick(item);
  };
  
  const handleKeyDown = useKeyControl({
    ref:     popupRef,
    selected,
    setSelected,
    isPopupShown,
    setPopupShow,
    options: _options,
    handleSuggestionClick,
    scrollTop,
    setScrollTop,
    resetCombobox,
  });
  
  const onChange = ({target: {value}}) => {
    setScrollTop(0);
    setSelected(0);
    setValue(value || '');
    onInputChange(value);
    setPopupShow(true);
  };
  
  useEffect(() => {
    if (suggestedValue) {
      setValue(suggestedValue);
    } else {
      setValue(noOptionsSelectedText||'');
    }
  }, [suggestedValue]);
  
  useEffect(() => {
    if (popupRef && popupRef.current) {
      popupRef.current.scrollTop = scrollTop;
    }
  });
  
  // useEffect(() => console.log('from state', valueFromState), [valueFromState]);
  
  return (
    <div className={'input-container'}>
      {
        label && <span>{label}</span>
      }
      <Autocomplete
        
        popupIcon={null}
        closeIcon={null}
        className={'combobox'}
        noOptionsText={<div ref={popupRef}>No matches found</div>}
        inputValue={valueFromState}
        options={_options}
        getOptionLabel={(o) => o.name}
        loading={isFetching}
        loadingText={'Loading...'}
        open={isPopupShown}
        
        onKeyDown={handleKeyDown}
        ListboxComponent={React.forwardRef((props, ref) => (
            !isFetching
              ? <ul ref={popupRef} className={'suggestion-list'}
                    onScroll={scrollHandler}>
                {_options.map((item, index) => (
                  <MenuItem key={item.id || index}
                            className={(index) === selected ? 'selected' : ''}
                            onClick={() => handleSuggestionClick(item)}
                  >{item.name || ' '}</MenuItem>
                ))}
              </ul>
              : null
          
          ),
        )}
        renderInput={params => (
          <TextField {...params} ref={inputRef} onChange={onChange}
                     name={inputName}
                     variant={variant || 'standard'}
                     autoComplete={'off'}
                     // helperText={withHelper && ' ' || ''}
                     value={valueFromState}
                     // InputProps={{ ...params.InputProps, type: 'search' }}
                     fullWidth
          />
        )}
        disabled={disabled}
        {...rest}
      
      />
    </div>
  );
};