import React from 'react';

export const ErrorPage = props => {
  return (
    <>
      <h1>Whooops, 404</h1>
      <p>Page, you are looking for, not found!</p>
    </>
  );
};