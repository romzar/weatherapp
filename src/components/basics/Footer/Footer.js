import React from 'react';

export class Footer extends React.Component {
  render () {
    return (
      <div className={'footer'}>
        <a href="https://www.qualium-systems.com">
          https://www.qualium-systems.com
        </a>
        <span>info@qualium-systems.com</span>
      </div>
    );
  }
}
