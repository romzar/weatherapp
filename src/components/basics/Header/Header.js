import AppBar                         from '@material-ui/core/AppBar';
import Toolbar                        from '@material-ui/core/Toolbar';
import _                              from 'lodash';
import React, { useEffect }           from 'react';
import { useTranslation }             from 'react-i18next';
import { useDispatch, useSelector }   from 'react-redux';
import { withRouter }                 from 'react-router-dom';
import { StringParam, useQueryParam } from 'use-query-params';
import { SearchCountryForm }          from '../../../containers/SearchCountryForm';
import { locales }                    from '../../../lib/i18n/constants';
import { LocalStorage }               from '../../../lib/LocalStorage';
import { CountryActions }             from '../../../store/actions/CountryActions';
import { UserActions }                from '../../../store/actions/UserActions';
import { WeatherActions }             from '../../../store/actions/WeatherActions';
import { AuthSection }                from '../../AuthSection';
import { CustomSelect }               from '../../common/InputControls/Select';
import { UserSection }                from '../../UserSection';

const Header = props => {
  let {history, location}               = props;
  const {isAuthorized, profile}         = useSelector(store => store.user);
  const dispatch                        = useDispatch();
  const [countryParam, setCountryParam] = useQueryParam('country', StringParam);
  let {i18n}                            = useTranslation();
  
  useEffect(() => {
    dispatch(UserActions.getCurrentUserInfo());
  }, []);
  
  useEffect(() => {
    if (!_.isUndefined(countryParam)) {
      console.log(countryParam);
      
      dispatch(CountryActions.fetchCountryInfo(countryParam))
        .then(
          country => country && dispatch(WeatherActions.fetchWeather(country.capital)),
        );
    } else {
      dispatch(CountryActions.fetchCurrentCountryInfo())
        .then(
          country => country && dispatch(WeatherActions.fetchWeather(country.capital)),
        );
    }
  }, [countryParam]);
  
  const selectSuggestionHandler = country => {
    if (!country) return;
    if (location.pathname !== `/${i18n.language}/`) {
      history.push(`/${i18n.language}/?country=${country.name}`);
    } else {
      setCountryParam(country.name);
    }
  };
  return (
    <AppBar position="static" className={'header'}>
      <Toolbar>
        <CustomSelect
          options={locales.map(locale => ({id: locale, name: locale}))}
          value={i18n.language}
          onChange={(e, o) => {
            console.log(o);
            i18n.changeLanguage(o.props.value);
          }}
        />
        <SearchCountryForm
          value={countryParam || null}
          onClick={selectSuggestionHandler}/>
        
          
            <UserSection
              isAuthorized={isAuthorized}
              firstName={profile.firstName}
              lastName={profile.lastName}
              logo={profile.logo}
              purgeUser={() => {
                LocalStorage.setAuthToken('');
                dispatch(UserActions.purgeUser());
              }}/>
            <AuthSection
              isAuthorized={isAuthorized}
            />
        
      </Toolbar>
    </AppBar>
  );
};

export default withRouter(Header);