import IconButton from '@material-ui/core/IconButton';
import AddIcon    from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import React      from 'react';

export const CustomAvatar = props => {
  let {src, alt, onAdd, onDelete} = props;
  return (
    <div className={'custom-avatar'}>
      <img alt={alt || ''} src={src}/>
      <div className={'overlay'}>
        <IconButton onClick={onAdd} className={'add-btn'}>
          <AddIcon fontSize={'inherit'} style={{fill: 'white'}}/>
        </IconButton>
        <IconButton onClick={onDelete} className={'delete-btn'}>
          <DeleteIcon fontSize={'inherit'} style={{fill: '#c00'}}/>
        </IconButton>
      </div>
    </div>
  );
};