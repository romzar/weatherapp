import _                   from 'lodash';
import React               from 'react';
import { NumberFormatter } from '../../../lib/NumberFormatter';
import { TableItem }       from './TableItem';
import {i18n}              from '../../../lib/i18n/ingex';

export class CountryInfoTable extends React.Component {
  
  render () {
    let {value} = this.props;
    return (
      <div className={'country-info-table'}>
        <TableItem
          label={i18n.t('Capital')}
          value={value.capital}
        />
        <TableItem
          label={i18n.t('Currency')}
          value={`${_.head(value.currencies).name}, ${_.head(value.currencies).symbol}`}
        />
        <TableItem
          label={i18n.t('Language')}
          value={_.head(value.languages).name}
        />
        <TableItem
          label={i18n.t('Population')}
          value={NumberFormatter.toLocale(value.population)}
        />
        <TableItem
          label={i18n.t('Region')}
          value={value.region}
        />
      </div>
    );
  }
}