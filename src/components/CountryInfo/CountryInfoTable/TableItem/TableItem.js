import React from 'react';

export class TableItem extends React.Component {
  
  render () {
    const {label, value} = this.props;
    return (
      <div className={'country-table-item'}>
        <span>{label}:</span>
        <span>{value}</span>
      </div>
    );
  }
}