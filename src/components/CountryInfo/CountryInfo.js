import React                from 'react';
import { CustomLoader }     from '../CustomLoader';
import { CountryInfoTable } from './CountryInfoTable';

export class CountryInfo extends React.Component {
  render () {
    let {isFetching, country, isError} = this.props.value;
    return (
      <div className="country-info">
        {!isError
          ? (
            !isFetching
              ? <>
                <div className={'heading'}>
                  <img alt={country.name} width="200" src={country.flag}/>
                  <h2>{country.name}</h2>
                </div>
                <CountryInfoTable value={country}/>
              </>
              : <CustomLoader/>
          )
          : null
          
        }
      </div>
    );
  }
}