import React              from 'react';
import { useTranslation } from 'react-i18next';
import { Link }           from 'react-router-dom';

export const AuthSection = (props) => {
  const {i18n}       = useTranslation();
  let {isAuthorized} = props;
  return (
    
    !isAuthorized
      ? (
        <div className={'auth-section'}>
          <Link to={`/${i18n.language}/?modal=login`}>
            Login
          </Link>
          < Link to={`/${i18n.language}/?modal=signup`}>
            Sign Up
          </Link>
        </div>
      )
      : null
  
  );
};

