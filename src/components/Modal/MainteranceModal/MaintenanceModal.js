import React            from 'react';
import { CustomLoader } from '../../CustomLoader';

export const MaintenanceModal = props => {
  
  return (
    <div className={'modal blocker-modal'}>
      <h1>This site is under maintenance</h1>
      <p>Page will refresh automatically</p>
      <CustomLoader/>
    </div>
  );
};