import { Dialog, useMediaQuery, useTheme } from '@material-ui/core';
import React                               from 'react';

export const Modal = props => {
  const {isOpen, handleClose, component} = props;
  const theme                 = useTheme();
  const fullScreen            = useMediaQuery(theme.breakpoints.down('sm'));
  return (
    <Dialog
      fullScreen={fullScreen}
      className={'modal'}
      open={isOpen}
      onClose={handleClose}
      closeAfterTransition
    >
      {component}
    </Dialog>
  );
};