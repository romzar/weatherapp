import React                            from 'react';
import { useTranslation }               from 'react-i18next';
import { useSelector }                  from 'react-redux';
import { Redirect, Route, useLocation } from 'react-router-dom';

export const ProtectedRoute = ({component: Component, ...rest}) => {
  const isAuth   = useSelector(store => store.user.isAuthorized);
  const {i18n}   = useTranslation();
  const location = useLocation();
  
  return (
    <Route {...rest} render={(props) => (
      isAuth
        ? <Component {...props}/>
        : <Redirect
          to={{
            pathname: `/${i18n.language}/`,
            state:    {
              from: location,
            },
          }}
        />
    )}/>);
};