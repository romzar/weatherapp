import React              from 'react';
import { useTranslation } from 'react-i18next';
import { Route, Switch }  from 'react-router-dom';
import { ErrorPage }      from '../components/ErrorPage';
import MainPage           from '../containers/MainPage/MainPage';
import UserInfoPage       from '../containers/UserInfoPage/UserInfoPage';
import { useUrlLocale }   from '../lib/hooks';
import { ProtectedRoute } from './ProtectedRoute';

export default (props) => {
  const {i18n} = useTranslation();
  
  useUrlLocale();
  
  return(
    <Switch>
      <Route path={`/${i18n.language}/`} exact component={MainPage}/>
      <ProtectedRoute exact path={`/${i18n.language}/user`}
                      component={UserInfoPage}/>
      <Route component={ErrorPage}/>
    </Switch>
  );
}