import _ from 'lodash';

export class CustomError extends Error {
  constructor (errObj) {
    super();
    _.keys(errObj).forEach(field => {
      this[field] = errObj[field];
    });
  }
}