import axios                                  from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { Provider }                           from 'react-redux';
import { Route }                              from 'react-router';
import { BrowserRouter as Router }            from 'react-router-dom';
import { toast }                              from 'react-toastify';
import { PersistGate }                        from 'redux-persist/integration/react';
import { QueryParamProvider }                 from 'use-query-params';
import './App.css';
import { Footer }                             from './components/basics/Footer';
import { Header }                             from './components/basics/Header';
import { MaintenanceModal }                   from './components/Modal/MainteranceModal';
import { Blocker }                            from './lib/Blocker';
import { instance }                           from './lib/di';
import { HttpClient }                         from './lib/HttpClient/HttpClient';
import { Routes }                             from './routes';
import { persistor, store }                   from './store';
import { UserActions }                        from './store/actions/UserActions';
import './styles/index.scss';

function App () {
  HttpClient.setDispatch(store.dispatch);
  HttpClient.setNotifier(toast);
  HttpClient.setHttpClient(axios);
  
  let appRef = useRef(null);
  
  let BlockUI = instance(Blocker).Render;
  
  const [blockerShow, setBlockerShow] = useState(false);
  useEffect(() => {
    UserActions.getCurrentUserInfo();
    
    store.subscribe(() => {
      setBlockerShow(!(store.getState().server.isApiEnabled));
    });
  }, []);
  
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>
          <QueryParamProvider ReactRouterRoute={Route}>
            <div ref={appRef} className="App">
              <Header/>
              <Routes/>
              <Footer/>
              <BlockUI isOpen={blockerShow} wrapperRef={appRef}
                       Component={MaintenanceModal}/>
            </div>
          </QueryParamProvider>
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
