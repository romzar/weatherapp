//
import React, { useEffect }        from 'react';
import { useHistory, useLocation } from 'react-router-dom';

export class Blocker {
  show (openCb) {
    let wrapperParent = this._wrapper.current.parentNode;
    let wrapperNextSibling = this._wrapper.current.nextSibling;
    let overlay = document.createElement('div');
    
    overlay.addEventListener('keydown', (e) => {
      e.key === 'Tab' && e.preventDefault();
    });
    overlay.classList.add('overlay');
    wrapperParent.insertBefore(overlay, wrapperNextSibling);
    
    
    overlay.setAttribute('tabindex', '0');
    overlay.focus();
    
    this._overlay = overlay;
  
    wrapperParent.classList.add('blocked');
    
    if (openCb) {
      this._timeoutId && clearTimeout(this._timeoutId);
      
      let timeoutCb = async () => {
        await openCb();
        this._timeoutId = setTimeout(timeoutCb, 5000);
      };
      
      this._timeoutId = setTimeout(timeoutCb, 5000);
    }
  }
  
  close () {
    this._overlay && this._wrapper.current.parentNode.removeChild(this._overlay);
    // let wrapperClassList = _.get(this._wrapper, 'current.classList', {});
    // wrapperClassList.remove('overlay');
    this._wrapper.current.parentNode.classList.remove('blocked');
    
    this._timeoutId && clearTimeout(this._timeoutId);
    this._history && this._history.replace(this._location.pathname + this._location.search);
  }
  
  Render = (props) => {
    let {Component, wrapperRef, isOpen, onOpen} = props;
    
    const history  = useHistory();
    const location = useLocation();
    
    useEffect(() => {
      this._history  = history;
      this._wrapper  = wrapperRef;
      this._location = location;
    }, [location, history]);
    
    useEffect(() => {
      isOpen
        ? this.show(onOpen)
        : this.close();
    }, [isOpen]);
    
    return (
      isOpen && <Component onBlur={(e) => e.target.focus()}/>
    );
  };
}