import { useState } from 'react';

export const useOffsetPagination = (cb)  => {
  const [scrollTop, setScrollTop] = useState(0);
  
  const scrollHandler = ({target}) => {
    if (target.scrollHeight === target.scrollTop + target.clientHeight) {
      setScrollTop(target.scrollTop);
      cb && cb();
      
    }
  };
  return {scrollTop, setScrollTop, scrollHandler};
};