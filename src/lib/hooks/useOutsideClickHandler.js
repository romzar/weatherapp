import { useEffect } from 'react';



export const useOutsideClickHandler = (refs = [], cb) => {

  const handleClickOutside = (event) => {
    console.log(refs);
    if (!refs.some(ref => ref.current && ref.current.contains(event.target))) {
      cb && cb();
    }
  };
  
  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });
};