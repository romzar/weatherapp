const scrollToSelected = (selected, setScrollTop, ref) => {
  if (!ref.current) return;
  let element = ref.current;
  if (element.scrollTop + element.clientHeight < element.children[selected].offsetTop + element.children[selected].clientHeight) {
    setScrollTop(element.children[selected].offsetTop + element.children[selected].clientHeight - element.clientHeight);
  }
  if (element.scrollTop > element.children[selected].offsetTop) {
    setScrollTop(element.children[selected].offsetTop);
  }
};

const KEY_CODES = (e) => {
  return {
    [e.key]: () => {},
    
    'ArrowUp': (props) => {
      let {setSelected, selected, isPopupShown, setScrollTop, ref} = props;
      
      isPopupShown && setSelected(selected !== 0 ? --selected : selected);
      scrollToSelected(selected, setScrollTop, ref);
      
    },
    
    'ArrowDown': (props) => {
      let {setSelected, selected, options, isPopupShown, setScrollTop, ref} = props;
      
      isPopupShown && setSelected((selected === options.length - 1) ? selected : ++selected);
      scrollToSelected(selected, setScrollTop, ref);
      
      // if (ref.current.scrollTop + ref.current.clientHeight < ref.current.children[selected].offsetTop + ref.current.children[selected].clientHeight) {
      //   props.setScrollTop(ref.current.children[selected].offsetTop + ref.current.children[selected].clientHeight - ref.current.clientHeight);
      //   console.log('111111');
      // }
      // if (ref.current.scrollTop > ref.current.children[selected].offsetTop) {
      //   props.setScrollTop(ref.current.children[selected].offsetTop);
      //   console.log('2222');
      // }
    },
    
    'Enter': (props) => {
      let {options, handleSuggestionClick, selected} = props;
      
      e.preventDefault();
      handleSuggestionClick(options[selected]);
    },
    
    'Tab': (props) => {
      let {resetCombobox} = props;
      resetCombobox();
    },
    
  }[e.key];
};

export const useKeyControl = (props) => {
  return (e) => {
    KEY_CODES(e)(props);
  };
};