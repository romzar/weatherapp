import _                  from 'lodash';
import { toast }          from 'react-toastify';
import {VALID_LOGO_TYPES} from '../../configs';

export const useImageUpload = () => {
  // const uploadImage = (setValue, fieldname) => {
  const uploadImage = (cb) => {
    let input  = document.createElement('input');
    input.type = 'file';
    
    input.addEventListener('change', () => {
      try {
        let file = _.first(input.files);
        console.log(file);
        if (!file) return;
        if (!VALID_LOGO_TYPES.includes(file.type)) {
          file = {};
          toast.error('Invalid logo image type!');
        }
        // setValue(fieldname, file);
        cb(file);
      } catch (e) {
        console.log(e);
      }
    });
    
    input.click();
  };
  
  return uploadImage;
};

