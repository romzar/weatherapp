import _                           from 'lodash';
import { useEffect }               from 'react';
import { useTranslation }          from 'react-i18next';
import { useHistory, useLocation } from 'react-router';
import { locales }                 from '../i18n/constants';

export const useUrlLocale = () => {
  const {i18n}   = useTranslation();
  const location = useLocation();
  const history  = useHistory();
  
  useEffect(() => {
    let urlLocale = _.nth(location.pathname.split('/'), 1);
    
    if (i18n.language === urlLocale) return;
    
    if (!locales.includes(urlLocale)) {
      history.replace(`/${i18n.language}${location.pathname}${location.search}`);
      return;
    }
    
    let temp = location.pathname.split('/');
    temp[1]  = i18n.language;
    let url  = temp.join('/') + location.search;
    history.replace(url);
    
  }, [location, i18n.language]);
  
};