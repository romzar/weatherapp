import { HttpClient } from '../HttpClient/HttpClient';

export class CurrentLocationApi extends HttpClient {
  get isAuth () {return false}
  
  async getCurrentLocationInfo () {
    let {data} = await this.get();
    return data;
  }
}