import { HttpClient } from '../HttpClient/HttpClient';

export class UserLocationApi extends HttpClient {
  getRegions () {
    return this.get('/location/regions');
  }
  
  getCities (regionId, suggestion='', offset=0) {
    return this.get(
      '/location/cities',
      {
        regionId,
        suggestion,
        offset,
      });
  }
}