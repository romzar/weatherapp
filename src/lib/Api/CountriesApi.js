import _           from 'lodash';
import { SETTING }    from '../../configs/settings';
import { HttpClient } from '../HttpClient/HttpClient';

export class CountriesApi extends HttpClient {
  get isAuth () {return false}
  
  async getCountriesSuggestions (countryName, fields = SETTING.COUNTRY_INFO_FIELDS) {
    fields     = fields.join(';');
    let {data} = await this.get(`/name/${countryName}`, {fields});
    if (_.isArray(data))
      return _.take(data, SETTING.COUNT_SUGGESTIONS);
    else
      throw new Error(data.message);
  }
  
  async getCountryInfoByCode (code = '', fields = SETTING.COUNTRY_INFO_FIELDS) {
    fields     = fields.join(';');
    let {data} = await this.get(`/alpha/${code}`, {fields});
    return data;
  }
  async getCountryInfoByName (countryName, fields = SETTING.COUNTRY_INFO_FIELDS) {
    fields     = fields.join(';');
    let {data} = await this.get(`/name/${countryName}`, {fields});
    return _.first(data);
  }
}