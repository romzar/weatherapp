import { LocalStorage } from '../LocalStorage';
import { HttpClient }   from '../HttpClient/HttpClient';

export class UserApi extends HttpClient {
  
  logIn (data) {
    return this.post('/auth/login', data, false);
  }
  
  signUp (data) {
    data['role']    = 'CLIENT';
    let isMultipart = !!data.logo.name;
    
    return this.post('/auth/register', data, isMultipart);
  }
  
  getCurrentUserInfo () {
    return this.get('/me', {}, {});
  }
  
  updateUserInfo (data, infoType) {
    let temp = {...data};
  
    return this.post(`/users/${infoType}/update`, temp, !!temp.logo, {
      authorization: this.token,
    });
  }
  
  getHealth () {
    return this.get('/health');
  }
  
}