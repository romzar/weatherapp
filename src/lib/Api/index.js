import { SETTING }            from '../../configs/settings';
import { instance }           from '../di';
import { CountriesApi }       from './CountriesApi';
import { CurrentLocationApi } from './CurrentLocationApi';
import { UserApi }            from './UserApi';
import { UserLocationApi }    from './UserLocationApi';
import { WeatherApi }         from './WeatherApi';


export const APIs = {
  CountriesApi:       instance(CountriesApi, SETTING.COUNTRIES_ROOT_API_URL),
  WeatherAPI:         new WeatherApi(SETTING.WEATHER_ROOT_API_URL),
  CurrentLocationApi: new CurrentLocationApi(SETTING.LOCATION_ROOT_API_URL),
  UserApi:            new UserApi(SETTING.USERDATA_ROOT_API_URL),
  UserLocationApi:    new UserLocationApi(SETTING.USERDATA_ROOT_API_URL)
};
