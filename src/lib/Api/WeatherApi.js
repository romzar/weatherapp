import _              from 'lodash';
import { HttpClient } from '../HttpClient/HttpClient';

export class WeatherApi extends HttpClient {
  
  get isAuth () {return false}
  
  _getWoeid (cityName) {
    let url = `/location/search/`;
    return this.get(url, {query: cityName});
  }
  
  async getWeatherForecast (cityName) {
    const response = await this._getWoeid(cityName);
    let url        = `/location/${_.first(response.data).woeid}/`;
    let {data}     = await this.get(url);
    return data;
  }
}

