import { LoginForm }          from '../../components/forms/LoginForm';
import { SignUpForm }         from '../../components/forms/SignUpForm';
import { MODAL_FORM_ACTIONS } from '../../store/actions/ModalFormActions';
import React                  from 'react';
export const MAP_STATE_TO_MODAL = type => {
  return {
    [type]:                                () => {},
    [MODAL_FORM_ACTIONS.OPEN_LOGIN_FORM]:  (props) => {
      return (<LoginForm {...props}/>);
    },
    [MODAL_FORM_ACTIONS.OPEN_SIGNUP_FORM]: SignUpForm,
  }[type];
};

const MAP_LOCATION_TO_STATE = {
  login:  MODAL_FORM_ACTIONS.OPEN_LOGIN_FORM,
  signup: MODAL_FORM_ACTIONS.OPEN_SIGNUP_FORM,
};

export const mapLocationToDispatch = (location, modalActions) => {
  const [param, value] = location.search.substr(1).split('=');
  console.log(modalActions);
  if (param === 'modal' && !!value) {
    modalActions.openModal(MAP_LOCATION_TO_STATE[value]);
  } else {
    modalActions.closeModalForm();
  }
};

