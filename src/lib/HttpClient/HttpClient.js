import _                 from 'lodash';
import { toast }         from 'react-toastify';
import { CustomError }   from '../../errors';
import { ServerActions } from '../../store/actions/ServerActions';
import { LocalStorage }  from '../LocalStorage';



export class HttpClient {
  get token () {return LocalStorage.getAuthToken();}
  
  get isAuth () {return true;}
  
  static _dispatch   = null;
  static _notifier   = null;
  static _httpClient = null;
  
  static setDispatch (dispatch) {
    HttpClient._dispatch = dispatch;
  };
  
  static setNotifier (notifier) {
    HttpClient._notifier = notifier;
  }
  
  static setHttpClient (httpClient) {
    HttpClient._httpClient = httpClient;
  }
  

  constructor (host) {
    this.host = host;
  }
  
  _ServerErrorCallback = async (url, method, timeoutId) => {
    let {status} = await this.send({url, method});
    if (status === 200) {
      HttpClient._dispatch && HttpClient._dispatch(ServerActions.setApiEnabled());
      clearInterval(timeoutId);
    }
  };
  
  get ERRORS () {
    return {
      502: ({url, method, error}) => {
        HttpClient._dispatch && HttpClient._dispatch(ServerActions.setApiDisabled());
        console.log(url);
        let timeoutId = 0;
        let timeoutCb = async () => {
          let {status} = await this.send({url, method});
          if (status === 200) {
            HttpClient._dispatch && HttpClient._dispatch(ServerActions.setApiEnabled());
            clearTimeout(timeoutId);
          } else {
            timeoutId = setTimeout(timeoutCb, 5000);
          }
        };
        
        clearTimeout(timeoutId);
        timeoutId = setTimeout(timeoutCb, 5000);
        
        throw new Error('Server error!');
      },
      401: ({error}) => {
        throw new CustomError({
          message: error.response.data.message,
          status:  error.response.status,
        });
      },
      403: ({error}) => {
        throw new CustomError({
          message: error.response.data.message,
          status:  error.response.status,
        });
      },
      500: ({error}) => {
        HttpClient._notifier && HttpClient._notifier.error(error.response.data.message);
      },
    };
  }
  
  get (route = '', params = {}, headers = {}) {
    return this.request('get', route, {}, headers, params);
  }
  
  put (route = '', data = {}, params = {}, headers = {}) {
    return this.request(
      'put',
      route,
      data,
      headers,
      params,
    );
  }
  
  post (route = '', data = {}, isMultipart = false, headers = {}, params = {}) {
    let result      = data,
        contentType = 'application/json';
    
    if (isMultipart) {
      let formData = new FormData();
      
      _.keys(data).forEach((key) => {
        formData.set(key, data[key]);
      });
      
      result      = formData;
      contentType = 'multipart/form-data';
    }
    
    return this.request(
      'post',
      route,
      result,
      {
        ...headers,
        'Content-Type': contentType,
      }, params);
  }
  
  async request (method, url, data = {}, headers = {}, params = {}) {
    try {
      let res = await this.send({
        method,
        url:     this.host + url,
        data,
        headers: _.assign(
          headers,
          this.isAuth
            ? {authorization: this.token}
            : {},
        ),
        params,
      });
      
      return this._parseResponse(res);
      
    } catch (e) {
      this.ERRORS[e.response.status] && this.ERRORS[e.response.status]({
        url:    this.host + url,
        method: method,
        error:  e,
      });
    }
  }
  
  async send (request) {
    if (HttpClient._httpClient) {
      return HttpClient._httpClient(request);
    }
    throw new Error('Http request can not be sent, due to http client is not initialized');
  }
  
  _parseResponse (res) {
    if(_.isUndefined(res)) return {};
    let{data, status, headers} = res;
    return {
      data,
      status,
      options: {
        pagination: {
          //Offset pagination
          totalRecords: headers['X-TOTAL-RECORDS'] || headers['x-total-records'],
          totalPages:   headers['X-TOTAL-PAGES'] || headers['x-total-pages'],
          pageOffset:   headers['X-PAGE-OFFSET'] || headers['x-page-offset'],
          pageLimit:    headers['X-PAGE-LIMIT'] || headers['x-page-limit'],
          //Page pagination
          currentPage:  headers['X-PAGE-POS'] || headers['x-page-pos'],
          pageSize:     headers['X-PAGE-SIZE'] || headers['x-page-pos'],
        },
      },
    };
  }
}