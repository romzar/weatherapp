import _ from 'lodash';

export class LocalStorage {
  
  
  static set(field, data = {}, opt ={}) {
    localStorage.setItem(
      field,
      JSON.stringify(data, opt),
    );
  }
  
  static addChangedCountryWeather (country, changedData) {
    localStorage.setItem(
      country,
      JSON.stringify(changedData)
    );
  }
  
  static getChangedCountryWeather (country) {
    const countryInfo = localStorage.getItem(country);
    return JSON.parse(countryInfo);
  }
  
  static setAuthToken (token) {
    localStorage.setItem('authToken', token);
  }
  
  static getAuthToken () {
    return localStorage.getItem('authToken');
  }
  
  static clear = () => {
    localStorage.clear();
  };
  
}
