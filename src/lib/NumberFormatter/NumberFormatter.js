import _ from 'lodash';

export class NumberFormatter {
  static toLocale = number => {
    let lang = _.get(navigator, 'languages', navigator.language);
    !_.isArray(lang) && (lang = [lang]);
    return new Intl.NumberFormat(_.first(lang)).format(number);
  };
}
