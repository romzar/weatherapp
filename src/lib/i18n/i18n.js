import i18n                 from 'i18next';
import detector             from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import translationEN        from './langs/en';
import translationRU        from './langs/ru';

i18n
  .use(detector)
  .use(initReactI18next)
  .init({
      resources:   {
        en: {
          translation: translationEN,
        },
        ru: {
          translation: translationRU,
        },
      },
      // lng:         'en',
      fallbackLng: 'en', // use en if detected lng is not available
      
      keySeparator: false, // we do not use keys in form messages.welcome
    whitelist:['en','ru'],
      interpolation: {
        escapeValue: false, // react already safes from xss
      },
      detection:     {
        order: ['path', 'localStorage','querystring', 'cookie',  'navigator', 'htmlTag',  'subdomain'],
        lookupFromPathIndex: 0,
      },
    },
  );

export default i18n;

