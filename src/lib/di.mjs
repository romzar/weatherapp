import axios  from 'axios'
import moment from 'moment';
let instances = new Map();

/**
 * Support function to control module instances
 */
export function instance (source) {
  if (!instances.has(source)) {
    let args = Array.from(arguments).slice(1);
    console.log('instance created');
    instances.set(source, new source(...args));
  }
  console.log('get instance');
  return instances.get(source);
}
//
// class Aaaa {
//   constructor (a, b) {
//     this.a = a;
//     this.b = b;
//   }
// }

//

// console.log(moment(undefined).toISOString());
// console.log(moment(null).toISOString());
// console.log(moment('').toISOString());

// let a = instance(Aaaa, 1, 2);
// console.log('-----------------');
// let b = instance(Aaaa, 2, 3);


  // axios.get('http://api.avtoboom.in.ua/')
  //   .catch((e)=>{
  //     //console.log(e);
  //     console.log(e.response);
  //   });

// catch(e){
//   console.log(e);
//   console.log(e.response);
// }


// const a = {
//   d: 3,
//   b: {
//     c: 3
//   }
// };
//
// const b = {
//   d: a,
//   b: {
//     c: 3
//   }
// };
//
// a.b.c = b;
//
//
// let d = JSON.parse(JSON.stringify(a));
//
// a.b.c = 10;
//
// console.log(d)





