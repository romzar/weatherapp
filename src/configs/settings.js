export const SETTING = {
  //MAX_SEARCHES: 10,
  COUNTRIES_ROOT_API_URL: 'https://restcountries.eu/rest/v2',
  WEATHER_ROOT_API_URL:   'https://www.metaweather.com/api',
  LOCATION_ROOT_API_URL:  'https://locate.now.sh/geo',
  USERDATA_ROOT_API_URL:  'https://api.avtoboom.in.ua/v1',
  COUNTRY_INFO_FIELDS:    ['name', 'flag', 'capital', 'languages', 'currencies', 'region', 'population'],
  COUNT_SUGGESTIONS:      5,
  MIN_SUGGESTION_LENGTH:  2,
  DEFAULT_LOCALE:         'ru',
};
